# FABApp

Este archivo README.md, osea el Readme, es un archivo de texto que esta escrito
formato markdown. El formato Markdown es muy sencillo de aprender, tanto que,
lo unico que necesitas para entenderlo, es ver el source code de este mismo documento.

## Uso

Se le puede utilizar para dar una descripcion general del proyecto.
En el caso de este proyecto, pretendo usarlo para llevar un control de las 
actividades que vamos realizando. La razon de esto es que es el primer archivo
que se carga cuando se abre el proyecto en la pagina de gitlab.

Las siguientes secciones explican los pasos que considero debemos seguir para 
completar el proyecto.

### Aprender a programar aplicaciones en Android

Es el punto central del proyecto, una aplicacion para Android cuyo codigo va a 
ser hosteado en este repositorio en GitLab.

La fuente principal de la cual deberiamos basarnos para aprender sugiero que sea
la siguiente:

[Espcializacion en Android por la Universidad de Vanderbilt](https://www.coursera.org/specializations/android-app-development)

El curso esta pensado para utilizar Android Studio. Es un IDE (Entorno de Desarrollo Integrado). El programa Android Studio contiene todo lo
necesario para desarrollar las aplicaciones Android.

La especializacion consta de 5 cursos, los cuales deberiamos de ver a la par.
Mas informacion con respecto a esto despues.

### Aprender a usar Git

GitLab es una plataforma en linea que hostea nuestro repo del proyecto, pero el sistema que se utiliza
para el repositorio se llama Git.
Mas informacion sobre como utilizar Git si es necesaria.

### Uso del SO para uso de GitLab y Git

En caso de ser necesario, aprender a usar Linux, para usar Git directamente en Linux. Android Studio puede correr tanto en Windows como en Linux.

Para utilizar Git en Windows se puede utilizar el programa TortoiseGit.
[Aqui](https://www.youtube.com/watch?v=30vtGKU1R2s) hay un tutorial basico que explica como usar tortoisegit en windows.

# Actividades necesarias para el desarrollo del proyecto.

Ya sea que lo hagamos mientras vemos los videos de los cursos, o despues, deberiamos
realizar las siguientes actividades.

- [X] Crear el proyecto de Android Studio inicial
- [X] Crear una aplicacion HolaMundo.
- [X] Correr la aplicacion en tu computadora local.
- [X] Subirla a GitLab.
- [X] Asegurarse de que la otra persona puede:
- [X] descargar el proyecto (hacerle checkout, o clonarlo) en su computadora local
- [X] Editar y compilar el proyecto (build)
- [X] Correrlo, probarlo
- [X] Subir los archivos modificados de vuelta a GitLab.

Este seria el workflow que estariamos teniendo para desarrollar el proyecto.


# Parte de Marco.

En este apartado estare indicando las dudas que se me presentaran a lo largo de los cursos sugeridos.
