package com.pack.audiolibros.audiobooks;

import android.media.MediaPlayer;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {
    /* Declare an instance of MediaPlayer */
    MediaPlayer player;

    /* The image files are stored in this array */
    int[] IMAGES = {R.drawable.ernest_hemingway, R.drawable.mark_twain, R.drawable.shakespeare, R.drawable.stephen_king,
                    R.drawable.virginia_woolf};

    /* Array for the names of the chapters. */
    String[] NAMES = {"Ernest Hemingway", "Mark Twain", "William Shakespeare", "Stephen King", "Virginia Woolf"};
    String[] DESCRIPTIONS = {"Writer", "Scientist", "Old man", "Indian", "Woman"};

    /* Array of audio files that will be played when we click on a list element. */
    int[] SONGS = {
            R.raw.bensoundallthat,
            R.raw.bensoundjazzcomedy,
            R.raw.bensoundscifi,
            R.raw.bensoundtheelevatorbossanova,
            R.raw.bensoundthejazzpiano
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        /* Create ListView object when the activity is created */
        ListView listView = (ListView)findViewById(R.id.capitulo);

        /* Create object for the custom adapter */
        CustomAdapter customAdapter = new CustomAdapter();

        /* bind custom adapter to the list view */
        listView.setAdapter(customAdapter);


        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                // This code executes when an element in the list is clicked.
                // We use position to index into the audio files.

                // Stop any file that has been playing so far.
                stop();
                // Now play the song according to which element was pressed.
                play(SONGS[position]);
            }
        });
    }

    /*
    ************************************************************************************************
     Code for the MediaPlayer class
    Create the three methods for the buttons
    in the user interface. Make sure they are public.
    ************************************************************************************************
     */

    // uncomment when audio functionality is implemented again
    public void play(int song_id) {
        // Create a MediaPlayer object if it hasn't been created
        if (player == null) {
            // Need to pass a context, and the name of the audio file we want to play.
            player = MediaPlayer.create(this, song_id);

            // Set an onCompletionListener, so that a callback function is called when the audio file finishes
            // playing. When this function is executed, it should release the system resources, by way of
            // calling the stopPlayer method.
            player.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
                @Override
                public void onCompletion(MediaPlayer mp) {
                    stopPlayer();
                    // Further work, play next song????
                    //
                }
            });
        }

        player.start();
    }

    public void pause() {
        // check that a player object has been created
        if (player != null) {
            player.pause();
        }
    }

    // In this method, we not only want to stop the media player, but we also want to release the resources.
    public void stop() {
        stopPlayer();
    }

    // Releases system resources from the MediaPlayer
    private void stopPlayer() {
        if (player != null) {
            // release resources and manually set the player variable to null.
            player.release();
            player = null;
            //Toast.makeText(this, "Capítulo detenido", Toast.LENGTH_SHORT).show();
        }
    }

    // Override the onStop function so that it also liberates the system resources when player is Stopped.
    @Override
    protected void onStop() {
        super.onStop();
    }


    /*
    ************************************************************************************************
     Code for the ListView class
    Create the three methods for the buttons
    in the user interface. Make sure they are public.
    ************************************************************************************************
     */
    class CustomAdapter extends BaseAdapter {

        @Override
        public int getCount() {
            return IMAGES.length;
        }

        @Override
        public Object getItem(int position) {
            return null;
        }

        @Override
        public long getItemId(int position) {
            return 0;
        }

        @Override
        public View getView(int position, View view, ViewGroup parent) {
            view = getLayoutInflater().inflate(R.layout.customlayout, null);

            ImageView imageView           = (ImageView)view.findViewById(R.id.imageView);
            TextView textView_name        = (TextView)view.findViewById(R.id.textView_name);
            TextView textView_description = (TextView)view.findViewById(R.id.textView_description);

            // Set the image of the view depending on the position of the view
            imageView.setImageResource(IMAGES[position]);
            // Set the name of the view
            textView_name.setText(NAMES[position]);
            textView_description.setText(DESCRIPTIONS[position]);

            return view;
        }
    }
}
