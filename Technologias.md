Este va a ser el archivo en el cual pongamos toda clase de informacion importante
relacionada a los puntos principales del proyecto.

Uno de los puntos centrales del proyecto es la capacidad para 
reproducir, pausar, detener, avanzar, y retroceder archivos de audio.

La opcion mas viable es buscar una forma nativa, es decir, no escribir nuestra propia
funcionalidad, sino utilizar la que Android provee por defecto.

# La clase MediaPlayer

[Este enlace](https://developer.android.com/reference/android/media/MediaPlayer)
contiene la documentacion sobre la clase MediaPlayer, que es la implementacion propia
de Android para la reproduccion de archivos multimedia en general.

[Este vide](https://www.youtube.com/watch?v=C_Ka7cKwXW0) muestra como implementar
paso a paso la clase MediaPlayer, permitiendo reproducir, pausar, y detener la reproduccion
de un archivo de audio.

[Este video](https://www.youtube.com/watch?v=pZjdrEmq36A) tambien muestra como implementar
la clase MediaPlayer, pero en este caso realiza una interfaz grafica con los botones
de play, pause, etc.

# Uilizar VCS (Version Control System) en Android Studio

Android Studio permite usar Git (un sistema de control de versiones (o VCS)).
Git es el mismo VCS que usa GitLab, y GitHub.

Para usar Git en Android Studio, primero hay que descargar Git para windows.

## Instalar Git para Windows

Click [Aqui](https://git-scm.com/download/win), para descargar Git para windows automaticamente.
Sigue todas las instrucciones y deja todo como el sistema lo sugiere.

## Crear un proyecto de android enlazado a gitlab
Se debe hacer lo siguiente:
- Configurar las credenciales de Git
- Ir a la carpeta donde se creará el proyecto de android
- clonar el repositorio de GitLab
- Meter el root de un nuevo proyecto de android dentro de la carpeta
- agregar la carpeta "git add carpetaConProyectoAndroid/"
- Commit "git commit -m "se agrego el proyecto android""
- push: "git push -u origin master"

Ahora deberiamos tener el esqueleto del proyecto en GitLab

## Introducir credenciales en git (gitbash)
Primero que nada, siempre debemos tener las credenciales de nombre y correo en git.
Abre gitbash.
Para introducir el nombre de usuario, es decir el que aparece en GitLab (el mio es "Jose Luis German")
$ git config --global user.name "YOUR_USERNAME"

Para introducir el correo electronico, correr en gitbash:
git config --global user.email "your_email_address@example.com"

Las credenciales se verifican con:
$ git config --global --list

Debe aparecer algo como:
user.name=Jose Luis German
user.email=luisgermanfelix@gmail.com

## Clonar el proyecto de GitLab, hacer cambios y subir

Seleccionar una carpeta en donde se vaya a clonar el proyecto, por ejemplo
Documents/AndroidApps
Posteriormente click derecho > Gitbash here
Se abrira gitbash e introducir el siguiente comando
git clone "https://gitlab.com/luisgermanfelix/fabapp.git"

Cuando las carpetas se hayan descargado, abrir AndroidStudio y abrir el proyecto que se acaba de descargar.
Cuando el proyecto haya cargado, Android studio preguntará si se quire agregar root para VCS (Git) Seleccionar Si.

Ahora Android sabrá que estamos usando el proyecto Android con Git.
En este punto, los archivos que vayamos modificando, se iran agregando automaticamente al staging area; de no ser asi,
se puede seleccionar manualmente cada archivo que se quiera agregar dando click derecho en el archivo > Git > Add.

Una vez que se hayan modificado los archivos deseados y se quieran subir los cambios a GitLab, dar click en el boton azul
"Commit Changes".
Una ventana mostrará los archivos modificados. Escribir un mensaje descriptivo y seleccionar la opcion "Commit and push".
Cuando Android termine de pushear los archivos un mensaje deberia aparecer "Push successful".

## Elementos graficos de la aplicacion

La aplicacion requiere de algunos elementos graficos como botones que indiquen al
usuario las diferentes funciones de la app, tales como navegar a capitulos, buscar
mas aplicaciones del autor, y posiblemente donaciones.

Para diseñar estos elementos graficos se puede utilizar inkscape, el cual es software
libre por lo que solo se requiere bajar el instalador y correr el programa.
Adicional, inkscape es multiplataforma, porloque puede correr en windows y linux.

Inkscape puede ser descargado desde [Aqui](https://inkscape.org/es/).