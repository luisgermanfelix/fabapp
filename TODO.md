# Aqui se enumeran todos los pendientes y cosas por hacer relacionadas al desarrollo
del proyecto.

## Diseño de elementos comunes de interfaz grafica.

Se planean diseñar los siguientes botones:

* [ ] Ir a capitulos
* [ ] Mas audiolibros
* [ ] Otras aplicaciones
* [ ] Donaciones
* [ ] Reiniciar datos de la aplicacion
* [ ] Quitar anuncios?
* [ ] Boton de play, o iniciar reproduccion
* [ ] Boton de pausa
* [ ] Boton Next
* [ ] Boton Previous
* [ ] Boton Stop
* [ ] Fondos de pantalla para la app, puede ser un fondo para todas las pantallas.

Una vez que se hayan diseñado los botones, se deben

* [ ] Integrar los botones al proyecto.

## Agregar Actividades adicionales

* [ ] Diseñar una actividad principal, desde la cual el usuario puede seleccionar las
siguientes actividades:
* [x] Ir a capitulos. Esta ya esta mas o menos hecha pero necesitaria cambios.
* [ ] Mas audiolibros
* [ ] Otras aplicaciones
* [ ] Reiniciar datos de aplicacion
* [ ] Quitar anuncios??

