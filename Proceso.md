# Proceso

En este archivo estoy anotando todos los pasos a seguir para la creacion de este proyecto,
ya que uno de los objetivos es realizar varias apps similares a esta.
El objetivo final podria ser el de automatizar el proceso de creacion de cada app.

1. Creacion del proyecto en Android.
    Esta parte ya se encuentra lo suficientemente documentada en tecnologias.md, pero
    en caso de que haya informacion adicional, se puede agregar aqui.

2. Implementacion de la clase MediaPlayer

    - Crear directorio /res/raw
    - Insertar los archivos de audio en ese directorio. Los archivos de audio pueden
    ser en formato *.mp3 o *.wav. Los nombres de los formatos solo pueden ser caracteres
    en minuscula y numeros.
    - Incluir informacion sobre la generacion del layout.xml
    - Actualmente la actividad principal utiliza LinearLayout, con 3 botones: play,
    pause, y stop. Cuando se defina una UI final se automatizara el proceso de creacion
    del layout.
    - Los 3 botones tienen su funcionalidad documentada en el codigo. Cuando se llegue
    a una funcionalidad final el proceso se puede automatizar.

3. Implementacion del layout ListView

El primer paso es agregar un ListView en activity_main.xml. Elegir un ID descriptivo
para cada elemento de la lista, como "capitulo".

Posteriormente, crear un archivo como por ejemplo customlayout.xml en la carpeta 
res/layout.
Este archivo va a contener los views que van a formar parte de cada elemento de la
lista. En la version actual se tienen dos textViews y un ImageView.

Las imagenes se guardaran en la carpeta res/drawable. Los nombres de los archivos 
no pueden tener letras mayusculas ni simbolos extraños.

Dentro de MainActivity.java, se crean los siguientes arrays:
- IMAGES[]: apunta a las imagenes que tenemos guardadas en la carpeta res/drawable.
- NAMES[]: Los nombres asociados a cada una de las imagenes. Estos no son los nombres
de los archivos como tal, si no nombres como "Jose Luis German".
- DESCRIPTIONS[]: Para el segundo textView, alguna descripcion asociada a la imagen.

Posteriormente en el onCreate de la clase, declarar un objeto listView, asociandolo
al ID del listView que creamos.
Despues, declarar un objeto de la clase CustomAdapter y enlazar el adapter al listView.

Posteriormente, creamos una clase customAdapter que herede de BaseAdapter.
Vamos a hacer un override de sus cuatro metodos getCount, getItem, getItemId, y getView.
Los metodos que se han modificado hasta el momento son los de getCount y getView.
La mayor parte de la funcionalidad sucede en getView.

## Testing

Actualmente no se me ocurre una manera automatica de hacer testing, pero la funcionalidad
principal se deberia probar de manera manual cada vez que se compile la app.

Cuando se compile un app, esta debera ser probada con al menos un dispositivo real
o simulado. Todas las funciones implementadas deberan ser probadas para comprobar
su correcto funcionamiento.
En el caso de archivos de audio, se pueden utilizar archivos de duracion mas corta
con fines de prueba, y luego cargar los archivos reales correspondientes a cada libro.